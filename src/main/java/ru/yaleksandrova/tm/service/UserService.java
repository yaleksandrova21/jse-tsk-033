package ru.yaleksandrova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.IUserRepository;
import ru.yaleksandrova.tm.api.sevice.IPropertyService;
import ru.yaleksandrova.tm.api.sevice.IUserService;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.empty.*;
import ru.yaleksandrova.tm.exception.entity.UserEmailExistsException;
import ru.yaleksandrova.tm.exception.entity.UserLoginExistsException;
import ru.yaleksandrova.tm.exception.entity.UserNotFoundException;
import ru.yaleksandrova.tm.model.User;
import ru.yaleksandrova.tm.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        final User user = new User();
        user.setLogin(login);
        setPasswordSetting(user, password);
        user.setEmail(email);
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (role == null) throw new EmptyRoleException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        final User user = new User();
        user.setLogin(login);
        setPasswordSetting(user, password);
        user.setEmail(email);
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    public User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User updateUserByLogin(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User setPassword(@Nullable String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        setPasswordSetting(user, password);
        return user;
    }

    @NotNull
    @Override
    public User setRole(@Nullable String id, @Nullable Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public boolean isLoginExist(String login) {
        return userRepository.findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public User lockUserByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeUserByLogin(login);
    }

    private void setPasswordSetting(@NotNull final User user, @NotNull final String password) {
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final String passwordHash = HashUtil.salt(password, iteration, secret);
        user.setPasswordHash(passwordHash);
    }

}
