package ru.yaleksandrova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.api.IService;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.exception.system.IndexIncorrectException;
import ru.yaleksandrova.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public void add(E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.add(entity);
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) throw new ProjectNotFoundException();
        repository.addAll(entities);
    }

    @NotNull
    @Override
    public void remove(E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int size() {
        return repository.size();
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public E findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.size()) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @NotNull
    @Override
    public E removeById(@NotNull String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return repository.removeById(id);
    }

    @NotNull
    @Override
    public E removeByIndex(@NotNull String userId, @NotNull Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return repository.removeByIndex(userId, index);
    }


}
