package ru.yaleksandrova.tm.exception.empty;

import ru.yaleksandrova.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role cannot be empty");
    }

}
