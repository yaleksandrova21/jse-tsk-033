package ru.yaleksandrova.tm.exception.empty;

import ru.yaleksandrova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password cannot be empty");
    }

}
