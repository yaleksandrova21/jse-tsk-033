package ru.yaleksandrova.tm.exception.empty;

import ru.yaleksandrova.tm.exception.AbstractException;

public class EmptyFullNameException extends AbstractException {

    public EmptyFullNameException() {
        super("Error! Full name cannot be empty");
    }

}
