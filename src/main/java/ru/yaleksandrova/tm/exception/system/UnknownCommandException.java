package ru.yaleksandrova.tm.exception.system;

import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use " + ApplicationConst.HELP + " for display list of terminal commands");
    }

}
