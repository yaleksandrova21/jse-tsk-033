package ru.yaleksandrova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.AbstractEntity;
import ru.yaleksandrova.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public Task findByName(@NotNull String userId, @NotNull String name) {
        for(Task task: list){
            if(name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull String userId, @NotNull String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        list.remove(task);
        return task;
    }

    @NotNull
    @Override
    public Task startByIndex(@NotNull String userId, @NotNull Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByName(@NotNull String userId, @NotNull String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startById(@NotNull String userId, @NotNull String id) {
        final Task task = findById(userId, id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull String userId, @NotNull String id) {
        final Task task = findById(userId, id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @NotNull
    @Override
    public Task finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull String userId, @NotNull String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final Task task = findById(userId, id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        @NotNull final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        return list.stream().filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public Task unbindTaskById(@NotNull String userId, @NotNull String taskId) {
        @NotNull final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        findAllTaskByProjectId(userId, projectId).forEach(o -> list.remove(o.getId()));
    }

}
