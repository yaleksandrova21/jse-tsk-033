package ru.yaleksandrova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner implements Runnable{

    @NotNull
    private final String PATH = "./";

    private final int INTERVAL = 3;

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(
                bootstrap.getCommandService().getArguments().stream()
                        .map(AbstractCommand::name)
                        .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run,0,INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o-> o.isFile()&&commands.contains(o.getName()))
                .forEach(o-> {
                    bootstrap.parseCommand(o.getName());
                    o.delete();
                });
    }

}

