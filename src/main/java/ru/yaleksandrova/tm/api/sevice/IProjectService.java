package ru.yaleksandrova.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import java.util.List;
import java.util.Comparator;

public interface IProjectService extends IOwnerService<Project> {

    @NotNull
    void create(@Nullable String userId, @Nullable String name);

    @NotNull
    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @NotNull
    Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description);

    @NotNull
    Project removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
