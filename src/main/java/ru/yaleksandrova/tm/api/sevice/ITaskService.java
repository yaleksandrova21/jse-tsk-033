package ru.yaleksandrova.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;
import java.util.Comparator;

public interface ITaskService extends IOwnerService<Task> {

    @NotNull
    void create(@Nullable String userId, @Nullable String name);

    @NotNull
    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @NotNull
    Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description);

    @NotNull
    Task removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
