package ru.yaleksandrova.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    Project removeById(@Nullable String userId, @Nullable String projectId);

}
