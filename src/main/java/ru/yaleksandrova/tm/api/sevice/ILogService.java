package ru.yaleksandrova.tm.api.sevice;

import org.jetbrains.annotations.Nullable;

public interface ILogService {

    void info(@Nullable String message);

    void command(@Nullable String message);

    void debug(@Nullable String message);

    void error(@Nullable Exception e);

}
