package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    @NotNull
    String getCurrentUserId();

    void setCurrentUserId(@Nullable String currentUserId);

}
